import tradersAPI from '../lib/traders.api'

// ################################ BestTraders ################################

Template.BestTraders.onCreated(function() {
  Meteor.subscribe('traders')
})

Template.BestTraders.helpers({
  traders: () => {
    let traders = tradersAPI.getTraders()
    let tradersComplete = []
    _.each(traders, (trader) => {
      trader.login = {username: trader.username}
      trader.picture = {thumbnail: "https://randomuser.me/api/portraits/thumb/men/17.jpg"}
      trader.performance = Math.ceil(Math.random() * 100 + 40)
      tradersComplete.push(trader)
    })
    return traders;
  },

  // displayRanking: (index) => {
  //   return index + 1;
  // }
})

Template.BestTraders.events({
  'click [data-action="user-profile"]': function (e, tpl) {
    FlowRouter.redirect(`/user/${this.login.username}`);
  }
})

// ############################## BestTradersLine ##############################

Template.BestTradersLine.helpers({
  getTraderAvatarURL: function() {
    console.log('hits', this);
    return this.login.email
  }
})
