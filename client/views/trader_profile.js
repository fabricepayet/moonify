import Chart from 'chart.js';
import _ from 'underscore';
import md5 from 'js-md5';

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import orderClientAPI from '../lib/orders.api'
import userAPI from '../lib/users.api'
import tradersAPI from '../lib/traders.api'

const Orders = new Mongo.Collection('orders');
const Balances = new Mongo.Collection('balances');

Template.TraderProfile.onCreated(function() {
  let username = FlowRouter.current().params.username;
  Meteor.subscribe('tickers');
  Meteor.subscribe('balances', {username});
  Meteor.subscribe('traders', {username});
  tradersAPI.updateDataForTrader(username);
  this.traderProfile = new ReactiveVar(null);
})

Template.TraderProfile.onRendered(function() {
  this.autorun(() => {
    let username = FlowRouter.current().params.username;
    let profile = tradersAPI.getProfileByUsername(username);
    this.traderProfile.set(profile)
  })
})

Template.TraderProfile.helpers({
  isMyProfile: () => userAPI.isCurrentUser(FlowRouter.current().params.username),

  getTraderProfile: () => Template.instance().traderProfile.get(),

  traderBalance: () => {
    let profile = Template.instance().traderProfile.get()
    if (!profile) {
      return;
    }
    let exchanges = Balances.findOne(profile._id);
    if (exchanges) {
      return exchanges.summary
    }
  },

  traderOpenOrders: () => {
    let profile = Template.instance().traderProfile.get()
    if (profile) {
      return profile.orders
    }
  }
})

Template.portfolio.onRendered(function () {
  if (!this.data) {
    return
  }
  // met à jour les données de l'utilisateur
  traderBalance = this.data
  if (!traderBalance) {
    return
  }
  var ctx = document.getElementById("portfolioChart").getContext('2d');
  let filteredBalances = _.omit(traderBalance, (val, key, obj) => val === '0.00' || val === 0)

  let colors = [];
  for (let balance in filteredBalances) {
    r = Math.floor(Math.random() * 200);
    g = Math.floor(Math.random() * 200);
    b = Math.floor(Math.random() * 200);
    color = 'rgb(' + r + ', ' + g + ', ' + b + ')';
    colors.push(color)
  }
  data = {
    datasets: [{
        data: _.values(filteredBalances),
        backgroundColor: colors
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: _.keys(filteredBalances),

  };
  var myPieChart = new Chart(ctx,{
    type: 'pie',
    data: data,
    options: {
      responsive: true,
      // maintainAspectRatio: false,
    }
  });
})

Template.lastOrders.helpers({
  lastOrders: function() {
    let openOrders = this;
    if (!openOrders) {
      return
    }
    let orders = _.map(openOrders, (value, key) => {
      let order = value.descr
      order.vol = value.vol
      return order
    });
    return orders;
  }
})

Template.tickerInfo.helpers({
  tickerInfos: () => orderClientAPI.getTickers()
});
