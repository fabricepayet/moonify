/* global Meteor */

import bittrex from 'node-bittrex-api';
import async from 'async';

import { BITTREX_KEY, BITTREX_SECRET } from '../../../config';

import exchangesAPI from '../exchanges';
import balancesAPI from '../balances';

const handleInvalidAPIKey = (userID) => {
  exchangesAPI.invalidExchangeConfigurationForUser('bittrex', userID);
};

const getParsedForResult = ({ Currency, Balance }, __callback) => {
  // TODO: be sure bittrex has the right config
  if (Currency === 'BTC') {
    __callback(null, {
      currency: Currency,
      balance: Balance,
      value: Balance,
    });
  } else {
    let market = null;
    if (Currency === 'USDT') {
      market = `${Currency}-BTC`;
    } else {
      market = `BTC-${Currency}`;
    }
    bittrex.getticker({ market }, (ticker, err) => {
      if (err) {
        __callback(err);
      } else {
        const { result } = ticker;
        let valueBTC = result.Last;
        if (Currency === 'USDT') {
          valueBTC = 1 / result.Last;
        }
        __callback(null, {
          currency: Currency,
          balance: Balance,
          value: valueBTC * Balance,
        });
      }
    });
  }
};

const updateBalanceForUser = (userID, _callback) => {
  console.log(`[BITTREX] updateBalanceForUser: ${userID}`);
  const config = exchangesAPI.getExchangeConfigurationForUser('bittrex', userID)
  if (!config) {
    return _callback(new Meteor.Error(`[BITTREX] config not found for user ${userID}`))
  }
  const { invalid, key, secret } = config
  if (invalid || !key || !secret) {
    return _callback(new Meteor.Error(`[BITTREX] invalid configuration for user ${userID}`))
  }
  bittrex.options({
    apikey: key,
    apisecret: secret,
  });
  bittrex.getbalances((data, err) => {
    if (err) {
      const { message, success } = err
      if (message === 'APIKEY_INVALID') {
        handleInvalidAPIKey(userID);
        return process.nextTick(() => {
          _callback(new Meteor.Error(`[BITTREX] configuration is invalid for user ${userID}`));
        });
      }
      _callback(new Meteor.Error(`[BITTREX] updateBalanceForUser error for user ${userID}: ${err}`));
    } else {
      const { success, message, result } = data;
      if (success) {
        async.map(result, getParsedForResult, (_err, parsedResults) => {
          console.log('>>>>parsedResults<<<<<<', parsedResults, err);
          if (_err) {
            _callback(_err);
          } else {
            balancesAPI.updateBalance(userID, 'bittrex', parsedResults);
            _callback(null);
          }
        });
      } else {
        _callback(new Meteor.Error(`[BITTREX] getBalance failed for user ${userID}: ${err}`));
      }
    }
  });
}

const bittrexAPI = {
  updateDataForUser: (userID, callback) => {
    console.log(`[BITTREX] updateDataForUser ${userID}`);
    updateBalanceForUser(userID, callback);
  },
};

export default bittrexAPI;
