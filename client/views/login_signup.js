// functions

function _isEmailAddressValid(email) {
  return false;
}

function _loginWithPassword(email, password, template) {
  Meteor.loginWithPassword(email, password, (error) => {
    if (error) {
      template.errorMessage.set(error.reason)
    } else {
      FlowRouter.go('/');
    }
  })
}

function _createUser(email, password, username, template) {
  if (!email || !password || !username) {
    template.errorMessage.set('Please complete all fields.')
    return;
  }

  Accounts.createUser({
    username,
    password,
    email
  }, (error) => {
    if (error) {
      console.log('error on signup', error);
      template.errorMessage.set(error.reason)
    } else {
      FlowRouter.go('/');
    }
  })
}

// Template Login

Template.Login.onCreated(function() {
  this.errorMessage = new ReactiveVar(null)
})

Template.Login.helpers({
  userEmail: function() {
    return Meteor.user().emails[0].address
  },

  errorMessage: () => {
    return Template.instance().errorMessage.get()
  }
})

Template.Login.events({
  'click [data-action="login"]': function (e, tpl) {
    let email = tpl.$('#email').val()
    let password = tpl.$('#password').val()
    _loginWithPassword(email, password, tpl)
  },

  'keyup': function (e, tpl) {
    tpl.errorMessage.set(null);
    if (e.keyCode == 13) {
      let email = tpl.$('#email').val()
      let password = tpl.$('#password').val()
      _loginWithPassword(email, password, tpl)
    }
  }
})

// Template Signup

Template.Signup.onCreated(function() {
  this.errorMessage = new ReactiveVar(null);
})

Template.Signup.helpers({
  errorMessage: () => {
    return Template.instance().errorMessage.get()
  },

  isFormValid: () => {
    return true
    // TODO
    let tpl = Template.instance()
    if(!tpl.view.isRendered){
      return false
    }
    return tpl.$('form input.invalid').length
  }
})

Template.Signup.events({
  'click [data-action="signup"]': (e, tpl) => {
    let email = tpl.$('#email').val()
    let password = tpl.$('#password').val()
    let username = tpl.$('#username').val()
    _createUser(email, password, username, tpl);
  },

  'keyup': (e, tpl) => {
    if (e.keyCode == 13) {
      let email = tpl.$('#email').val()
      let password = tpl.$('#password').val()
      let username = tpl.$('#username').val()
      _createUser(email, password, username, tpl);
    }
  },

  'blur #email': (e, tpl) => {
    let $email = tpl.$('#email');
    if (!_isEmailAddressValid($email.val())) {
      $email.addClass('invalid')
    }
  },

  'focus #email': (e, tpl) => {
    let $email = tpl.$('#email');
    $email.removeClass('invalid')
  }
})
