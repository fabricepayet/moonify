/* global Meteor Mongo */

const Traders = new Mongo.Collection('traders');

const tradersAPI = {
  getTraders() {
    return Traders.find().fetch();
  },

  getProfileByUsername: username => Traders.findOne({ username }),

  updateDataForTrader: username => Meteor.call('updateDataForTrader', username),
};

export default tradersAPI;
