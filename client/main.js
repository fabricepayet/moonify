Template.navBar.events({
  'click [data-action="logout"]': (e, tpl) => {
    Meteor.logout()
  }
})
