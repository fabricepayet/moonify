/* global Mongo Meteor */

import Fiber from 'fibers';
import md5 from 'js-md5';

import exchangesAPI from './exchanges'

const Traders = new Mongo.Collection('traders');

_getUserIDFromUsername = (username) => {
  let user = Meteor.users.findOne({username})
  return user._id
}

const tradersAPI = {
  addTrader: (user) => {
    const trader = {
      username: user.username,
      _id: user._id,
    }
    let email = user.emails[0].address
    trader.thumbnail = `https://www.gravatar.com/avatar/${md5(email)}`;
    Traders.insert(trader, (err) => {
      console.error('Error when inserting traders:', err)
    })
  },

  updateBalance: (userID, exchange, balance) => {

  },

  updateOpenOrders: (userID, openOrders) => {
    Traders.update(userID, {
      $set: {
        orders: openOrders,
      },
    }, { upsert: true });
  },

  updateDataForTrader: (username) => {
    let userID = _getUserIDFromUsername(username)
    exchangesAPI.updateDataForUser(userID)
  }
}

Meteor.publish('traders', () => Traders.find())

export default tradersAPI;
