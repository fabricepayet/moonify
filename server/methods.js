/* global Meteor */

import exchangesAPI from './lib/exchanges';

Meteor.methods({
  updateDataForCurrentUser: () => {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    exchangesAPI.updateDataForUser(Meteor.userId());
  },

  updateDataForTrader: (username) => {
    if (!username) {
      throw new Meteor.Error('missing-argument');
    }
    exchangesAPI.updateDataForUsername(username);
  },

  updateExchangeConfiguration: (exchange, key, secret) => {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    if (!exchange || !key || !secret) {
      throw new Meteor.Error('missing-argument');
    }
    exchangesAPI.updateExchangeConfiguration(exchange, key, secret);
  },
});
